# Installing binaries on the remarkable


## Initial setup

The root partition has very limit space available, so it makes sense to
establish a location within the `/home` partition in order to avoid future
issues. The default `$PATH` variable includes a location traditionally used for
this purpose under *nix, but which is not included in the system image. We can
remount a folder from the home directory to this location in order to take
advantage of this and make our local binaries available without having to modify
the `$PATH` variable.

```sh
# ON DEVICE

# Create the missing system directory and the /home counterpart
mkdir -p /usr/local/bin /home/root/.local/bin

# Add a binding to /etc/fstab to mount the bin directory on boot
echo "/home/root/.local/bin  /usr/local/bin  none  bind  0  2" >> /etc/fstab

# Mount the directory
mount -a
```

At this point you should be able to put any executable script or binary in
`~/.local/bin` and running it like any other command.


## Adding binaries

Binaries must be built for the ARMv7 architecture.

Once downloaded or built just drop them into `~/.local/bin` and ensure the file
is executable.

## After updates

The remarkable resets configuration in `/etc` on as a part of it's upgrade
process, so you will notice that binaries are no longer in your path afterwards.
Redo the initial setup to restore the mount in order to fix this.
