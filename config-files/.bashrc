# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

export EDITOR=vim

# Aliases for common tasks
alias ui-reset='systemctl restart xochitl'
alias backup-shell='ENV=$HOME/.borg.env sh'
