# REMARKABLE tablet setup and tweaks

This repo serves as a record of modifications and customizations I make to my
remarkable.

## Things done so far

- Added a place for [custom software](software/adding-binaries.md) to live.
- Setup [borg for backup](backup/borg.md) with back in time-functionality.
