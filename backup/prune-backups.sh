#!/usr/bin/env bash

set -o errexit          # Exit on failure
set -o nounset          # Disallow unset variables
set -o pipefail         # Do not forward error output to pipe

# Check env
if [ -x ${BORG_PRUNE_KEEP+x} ]; then
    echo "ERR: \$BORG_PRUNE_KEEP must be set, or this command will delete all your backups.";
    echo "Tip: run '$0 --dry-run' to see what borg will do with your config."
    exit 2;
fi

# Run prune
borg prune --stats --list ${BORG_PRUNE_KEEP}
