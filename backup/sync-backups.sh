#!/usr/bin/env bash

set -o errexit          # Exit on failure
set -o nounset          # Disallow unset variables
set -o pipefail         # Do not forward error output to pipe

if [ -t 1 ]; then
    INTERACTIVE_ARGS="--progress";
else
    INTERACTIVE_ARGS="";
fi

# Run rclone with borg repo lock
borg with-lock "${BORG_REPO}" \
    rclone sync $RCLONE_SYNC_ARGS $INTERACTIVE_ARGS \
        "${BORG_REPO}" "${RCLONE_REMOTE}/backup"
