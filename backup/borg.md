# Setting up borg backup on the remarkable

[Borg][] is a flexible and efficient backup program focused on deduplicated
snapshots, which allows recovery of deleted files and restoring files to
previous states.

As described in the [installation docs][], Arm-builds can be found [here][].
Download the ARMv7 build and drop it into `.local/bin` or wherever you put your
binaries. A successful install can be verified by running `borg --version`.

Note that Borg can be a bit slow to load.

[borg]: https://borgbackup.readthedocs.io/en/stable/
[installation docs]: https://borgbackup.readthedocs.io/en/stable/installation.html#pyinstaller-binary
[here]: https://borg.bauerj.eu/

## Step 1: Setup borg config and initialize a local repo

Borg can work with both local and remote repositories. For battery, performance
and reliability reasons I decided to work against a local repo rather than have
borg working over the network. The repo can be synced with a different tool
after the backup step, when network is available. That way you can still create
snapshots when wifi is not available, and borg does not have to do diffs over
the net.

Borg backups are (optionally, but recommended) encrypted, and require a
passphrase for all interactions. In order to allow automatic and scheduled
backups, and to simplify interacting with borg, I opted to place the password
and the repo location along with some custom config in
[this env-file](../config-files/.borg.env). Insert your passphrase in that file
and copy it to `~/.borg.env`. Then to set up:

```sh
# Create the backup folder and copy the prepared scripts to the remarkable
./backup/install-scripts.sh

# Ssh into the remarkable
ssh root@10.11.99.1

# Add an alias to your bashrc to enter the backup shell
# OBS: note the double >> to append instead of overwrite the file
echo "alias backup-shell='ENV=$HOME/.borg.env sh'" >> ~/.bashrc

# Reload the bash environment (you can also log out and reconnect)
source ~/.bashrc

# Enter the environment
backup-shell

# You should see the greeting now

# Initialize the repository
# you can use other encryption modes if you like, but this is what I used
borg init --encryption=repokey

# Make the first snapshot
create-backup.sh

# Verify that the backup was created
borg list

# Exit the environment
exit # or press ctrl+d
```

## Step 2: Setup systemd to backup and prune automatically

I ended up with a somewhat convoluted strategy for this, with two separate
services and path activation units for the backup. The reason is that I wanted
to avoid creating snapshots too frequently. Just adding a sleep after the backup
would mean that changes during the sleep-period would not be backed up until the
next change. I therefore decided to use a separate watcher service that would
indicate changes while the backup service is running. Have a look at the install
script to see how it's set up.

```sh
# Install and activate services
./backup/install-services.sh
```

The services are copied to the systemd configuration in /etc/. When the
remarkable receives updates these files will be wiped. I think the whole root
partition is recreated. The install script should be safe to rerun at any time,
so just run it again to restore the scripts.

The default timing is as follows:

- The watcher waits five seconds to schedule a backup
- The backup service waits five minutes after completing a backup before exiting
  (and possibly starting another one)
- The prune-script runs 15 minutes after boot, and then once every hour while
  the device is active.

## Step 3: Setup rclone to sync the backup archives elsewhere

Follow https://remarkablewiki.com/tips/rclone in order to set up rclone.

If you don't already have a place to copy the files to I suggest trying b2. You
get 10GB for free, which is more than enough for the remarkable.

In order to ensure that the copy is valid we lock the repo while syncing. I've
added a script taking care of this. It means that the back up is locked though,
so in order to interact with the backup you need to run `borg break-lock` first.

```sh
backup-shell
sync-backups.sh
```

TODO: run sync on a schedule.
