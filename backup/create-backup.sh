#!/usr/bin/env bash

set -o errexit          # Exit on failure
set -o nounset          # Disallow unset variables
set -o pipefail         # Do not forward error output to pipe

# Run backup
borg create --verbose --list --filter AME --stats \
    --patterns-from $BORG_REPO/filter.txt \
    ::"${BORG_CREATE_NAMING}" \
    "${BACKUP_SOURCE}";
