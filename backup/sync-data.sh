#!/usr/bin/env bash

set -o errexit          # Exit on failure
set -o nounset          # Disallow unset variables
set -o pipefail         # Do not forward error output to pipe

if [ -t 1 ]; then
    INTERACTIVE_ARGS="--progress";
else
    INTERACTIVE_ARGS="";
fi

# Run rclone with borg repo lock
rclone sync $RCLONE_SYNC_ARGS $INTERACTIVE_ARGS \
    --filter-from $BORG_REPO/filter.txt \
    "${BACKUP_SOURCE}" "${RCLONE_REMOTE_ENCRYPTED}/data"
