#!/usr/bin/env bash

set -o errexit          # Exit on failure
set -o nounset          # Disallow unset variables
set -o pipefail         # Do not forward error output to pipe

##
# This script just creates the necessary folders and copies some prepared
# scripts onto the remarkable.
#
# By default it connects to the remarkable over usb, but you can supply a
# different address as the first parameter if necessary, e.g. for connecting
# over wifi.
##

# get the full path to the folder containing the scripts
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

# execute remotely
SSH_REMOTE="${1:-root@10.11.99.1}"
ssh "$SSH_REMOTE" /bin/sh <<EOT

cd /home/root

#ensure scripts folder is present
mkdir -p backup/scripts

# write the filter file
cat > backup/filter.txt <<'EOS'
$(cat $SCRIPTPATH/filter.txt)
EOS

echo "Copied filter patterns to ~/backup/filter.txt"

# write scripts
cat > backup/scripts/create-backup.sh <<'EOS'
$(cat $SCRIPTPATH/create-backup.sh)
EOS
cat > backup/scripts/prune-backups.sh <<'EOS'
$(cat $SCRIPTPATH/prune-backups.sh)
EOS
cat > backup/scripts/sync-backups.sh <<'EOS'
$(cat $SCRIPTPATH/sync-backups.sh)
EOS
cat > backup/scripts/sync-data.sh <<'EOS'
$(cat $SCRIPTPATH/sync-data.sh)
EOS

# set the executable bit
chmod a+x backup/scripts/{create-backup,prune-backups,sync-backups,sync-data}.sh

echo "Scripts added to ~/backup/scripts"

EOT
