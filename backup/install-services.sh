#!/usr/bin/env bash

set -o errexit          # Exit on failure
set -o nounset          # Disallow unset variables
set -o pipefail         # Do not forward error output to pipe
#set -o xtrace           # Print executed statements

##
# This script sets up systemd services to trigger backups whenever files are
# updated.
#
# By default it connects to the remarkable over usb, but you can supply a
# different address as the first parameter if necessary, e.g. for connecting
# over wifi.
##

# execute remotely
SSH_REMOTE="${1:-root@10.11.99.1}"
ssh "$SSH_REMOTE" /bin/sh <<EOT

echo Installing backup watcher service
cat > /etc/systemd/system/borg-watcher.service <<EOS
[Unit]
Description=request backup when files have changed

[Service]
Type=oneshot
ExecStartPre=/bin/sleep 5
ExecStart=/bin/touch /run/borg-backup.request
EOS

echo Installing backup watcher path activation
cat > /etc/systemd/system/borg-watcher.path <<EOS
[Unit]
Description=watching xochitl data directory for changes

[Path]
PathModified=/home/root/.local/share/remarkable/xochitl

[Install]
WantedBy=multi-user.target
EOS

echo Installing backup service
cat > /etc/systemd/system/borg-backup.service <<EOS
[Unit]
Description=creating snapshot of the xochitl data directory

[Service]
Type=oneshot
WorkingDirectory=/home/root
ExecStart=/bin/sh -c "source ./.borg.env; backup/scripts/create-backup.sh"
ExecStartPre=/bin/rm /run/borg-backup.request
ExecStartPost=/bin/echo "Sleeping for five minutes" ; /bin/sleep 300
EOS

echo Installing backup path activation
cat > /etc/systemd/system/borg-backup.path <<EOS
[Unit]
Description=watching for backup requests.

[Path]
PathExists=/run/borg-backup.request

[Install]
WantedBy=multi-user.target
EOS

echo Installing backup pruning service
cat > /etc/systemd/system/borg-prune.service <<EOS
[Unit]
Description=pruning borg snapshots

[Service]
Type=oneshot
WorkingDirectory=/home/root
ExecStart=/bin/sh -c "source ./.borg.env; backup/scripts/prune-backups.sh"
EOS

echo Installing bakup pruning timer
cat > /etc/systemd/system/borg-prune.timer <<EOS
[Unit]
Description=prune snapshots every hour

[Timer]
OnBootSec=15min
OnUnitActiveSec=1hour

[Install]
WantedBy=timers.target
EOS

echo reloading systemd config
systemctl daemon-reload

echo enable and start activation units
systemctl enable borg-watcher.path borg-backup.path borg-prune.timer
systemctl start borg-watcher.path borg-backup.path borg-prune.timer
EOT
